# cookiecutter-ansible

The idea for this repository is to create a cookiecutter template for ansible.

## group_vars/

This directory contains variable files for specific hosts or groups of hosts.

### group_vars/all.yml

This yaml file defines items you want to apply to all hosts.

## inventory/

This directory contains the inventory file(s) which define the hosts (servers)
that Ansible will manage.

### inventory/hosts

This inventory file lists the servers you are managing.

## playbooks/

This directory contains your Ansible playbooks, which are YAML files that
define the tasks Ansible will perform.

## ansible.cfg

`ansible.cfg` is your configuration file. We have provided an empty one,
generate an example for your project that includeds your plugins.

```shell
ansible-config init --disabled -t all > ansible.cfg
```

The documenation can be found at https://docs.ansible.com/ansible/latest/reference_appendices/config.html